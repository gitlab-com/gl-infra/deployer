#!/bin/bash

authenticateGCP() {
  echo "Authenticating with GCP..."

  declare -r jwt_file='.ci_job_jwt_file'
  declare -r google_application_credentials='.gcp_temp_cred.json'

  echo ${GCLOUD_OIDC} >${jwt_file}
  gcloud config set project gitlab-ops
  gcloud iam workload-identity-pools \
    create-cred-config ${GCP_WORKLOAD_IDENTITY_PROVIDER} \
    --service-account="${SERVICE_ACCOUNT_EMAIL}" \
    --output-file="${google_application_credentials}" \
    --credential-source-file=${jwt_file}
  gcloud auth login --cred-file="${google_application_credentials}"
}

# For logging events
# https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/events/README.md
sendEvent() {
  if [[ -z "$ES_NONPROD_EVENTS_URL" || $ES_EVENT_OVERRIDE == "true" ]]; then
    echo "Warning: Not sending events because ES_NONPROD_EVENTS_URL is not set or ES_EVENT_OVERRIDE is set to true"
    return
  fi

  command -v curl >/dev/null 2>&1 ||
    {
      echo >&2 "sending events requires curl but it's not installed."
      return
    }

  MSG="$1"
  ENV="$2"
  STAGE="${3:-main}"

  if ! [[ "${ENV%-cny}" == "gprd" || "${ENV%-cny}" == "gstg" ]]; then
    echo "Unsupported job environment \"$ENV\" for sending events, skipping"
    return
  fi

  TS=$(date -u +%s000)
  USERNAME="${GITLAB_USER_LOGIN:-unknown}"
  SOURCE="${CI_JOB_URL:-unknown}"
  VERSION="${DEPLOY_VERSION:-not set}"
  DATA="
    {
      \"time\": \"$TS\",
      \"type\": \"deployment\",
      \"message\": \"$MSG\",
      \"env\": \"$ENV\",
      \"username\": \"$USERNAME\",
      \"source\": \"$SOURCE\",
      \"version\": \"$VERSION\",
      \"stage\": \"$STAGE\"
    }
  "

  if [[ "$CHECKMODE" == "true" ]]; then
    echo "Skipping event notification because checkmode is set, would send \"$DATA\""
    return
  fi

  echo "Sending event: \"$MSG\""
  curl -s -X POST \
    --retry 5 \
    --retry-max-time 20 \
    "$ES_NONPROD_EVENTS_URL/events-${ENV%-cny}/_doc" -H 'Content-Type: application/json' -d "$DATA" >/dev/null
}

cd "$CI_PROJECT_DIR/deploy-tooling" || exit 1

# Default CHECKMODE to false
export CHECKMODE="${CHECKMODE:-false}"

# If DEPLOY_VERSION nor POSTDEPLOY_MIGRATIONS are set and it isn't a command
# run we will assume that we are running in testing mode
if [[ -z "$CMD" ]] && [[ -z "$DEPLOY_VERSION" ]] && [[ -z "$POSTDEPLOY_MIGRATIONS" ]]; then
  export CHECKMODE="true"
  export DEPLOY_VERSION="15.9.202302020620-417cf860cf3.2d3de2ab500"

  echo "DEPLOY_VERSION was not set, forcing Ansible checkmode and setting the version to $DEPLOY_VERSION for testing"
fi

# CHECKMODE can be set to "--check" for backwards compatibility
if [[ "$CHECKMODE" == "true" ]] || [[ "$CHECKMODE" == "--check" ]]; then
  export CHECKMODE="true"

  unset SLACK_TOKEN     # Used for change-lock slack notifications, always disable in check-mode
  unset GRAFANA_API_KEY # Ensure we do not do any grafana annotations in check-mode

  export SLACK_WEBHOOK_TOKEN="$SLACK_WEBHOOK_TOKEN_TESTING" # Used for deployment notifications
  export CHANGE_LOCK_OVERRIDE="true"                        # Ensure changelock failures never block a check pipline

  echo "Running in checkmode, change-lock windows will be ignored and notifications will be sent to #announcements-test"
fi

# Deployer may be triggered with multiple environments in DEPLOY_ENVIRONMENT
# like `gstg,gprd-cny`. The CURRENT_DEPLOY_ENVIRONMENT should reflect the
# current environment that is being deployed.
#
# The environment for the pipeline job is set to JOB_ENVIRONMENT
# The stage for the pipeline job is set to JOB_STAGE
#
# For tools that expect an environment to be set to <env>[-cny]
# we create a var named CURRENT_DEPLOY_ENVIRONMENT_STAGE
if [[ -n "$JOB_ENVIRONMENT" && -n "$JOB_STAGE" ]]; then
  # JOB_ENVIRONMENT and JOB_STAGE are only set in
  # CI and are always set together
  export CURRENT_DEPLOY_ENVIRONMENT="$JOB_ENVIRONMENT"
  export STAGE="$JOB_STAGE"

  if [[ "$JOB_STAGE" != "main" ]]; then
    CURRENT_DEPLOY_ENVIRONMENT_STAGE="${JOB_ENVIRONMENT}-${JOB_STAGE}"
  else
    CURRENT_DEPLOY_ENVIRONMENT_STAGE="$JOB_ENVIRONMENT"
  fi

  export CURRENT_DEPLOY_ENVIRONMENT_STAGE
fi

if ! which gcloud >/dev/null 2>&1 ; then
  echo "skipping auth with gcp"
else
  authenticateGCP
fi

cat <<END
  JOB_ENVIRONMENT: $JOB_ENVIRONMENT
  CURRENT_DEPLOY_ENVIRONMENT: $CURRENT_DEPLOY_ENVIRONMENT
  JOB_STAGE: $JOB_STAGE
  STAGE: $STAGE

  CURRENT_DEPLOY_ENVIRONMENT_STAGE: $CURRENT_DEPLOY_ENVIRONMENT_STAGE
  DEPLOY_VERSION: $DEPLOY_VERSION
  DEPLOY_ENVIRONMENT: $DEPLOY_ENVIRONMENT
  CHECKMODE: $CHECKMODE

  Ruby: $(ruby --version)
  RubyGems: $(gem --version)
  Bundler: $(bundle --version)
END
