## Deployer

This is the repository that has the `gitlab-ci.yml` for GitLab.com deployments.

For more information see the
[release documentation for deployer](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/gitlab-com-deployer.md).

## Implementing Downstream Pipelines
There are currently two implementations of downstream pipelines in `.gitlab-ci.yml`.

One is via the Ansible playbook ([example](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/blob/04574fb43ea914e89bc8d89b52b812adf0d7d1a5/.gitlab-ci.yml#L160-164)), and another is via GitLab CI's native support for [multi-project pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html?tab=Multi-project+pipeline#trigger-a-downstream-pipeline-from-a-job-in-the-gitlab-ciyml-file) ([example](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/blob/04574fb43ea914e89bc8d89b52b812adf0d7d1a5/.gitlab-ci.yml#L290-292)).

For future implementations, aim to use the [Gitlab CI's native support way of multi-project pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html?tab=Multi-project+pipeline#trigger-a-downstream-pipeline-from-a-job-in-the-gitlab-ciyml-file) via the `trigger:` keyword.  This feature was added after this `deployer` project was initiated, so the Ansible playbooks to trigger pipelines are remnants of the past that we've not gotten around to refactor yet.
